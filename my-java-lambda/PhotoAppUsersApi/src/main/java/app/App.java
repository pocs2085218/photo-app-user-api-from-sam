package app;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.google.gson.Gson;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Handler for requests to Lambda function.
 */
public class App implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {

    @Override
    public APIGatewayProxyResponseEvent handleRequest(final APIGatewayProxyRequestEvent input, final Context context) {


    String requestBody = input.getBody();
    
    Gson gson = new Gson();
    
    Map<String, String> userDetails = gson.fromJson(requestBody, Map.class);
    
    userDetails.put("userId", UUID.randomUUID().toString());
    
// TODO process user details

Map returnValue = new HashMap();
returnValue.put("firstName",userDetails.get("firstName"));
returnValue.put("lastName",userDetails.get("lastName"));
returnValue.put("userId",userDetails.get("userId"));

APIGatewayProxyResponseEvent response = new APIGatewayProxyResponseEvent();

response.withStatusCode(200).withBody(gson.toJson(userDetails, Map.class));
    
    return response;
    }

}
